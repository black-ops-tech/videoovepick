package com.practice.videopickerandoverlay

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.database.Cursor
import android.media.session.MediaController
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.VideoView

class MainActivity : AppCompatActivity() {
    val REQUEST_CODE = 1
    lateinit var videoView1 : VideoView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn : Button = findViewById(R.id.btn)
        videoView1= findViewById(R.id.video_view)
        btn.setOnClickListener {
            pickVideo()
        }
    }

    private fun pickVideo() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_PICK
        startActivityForResult(Intent.createChooser(intent, "Select Video"),REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                when (requestCode) {
                    REQUEST_CODE -> {
                        val videoUri: Uri = data?.data!!
                        val videoPath = parsePath(videoUri)
                        Log.d("TAG", "$videoPath is the path that you need...")

                        videoView1.setVideoPath(
                            videoPath)
                        videoView1.setOnPreparedListener { mp ->
                            mp.isLooping = true
                            Log.i(TAG, "Duration = " + videoView1.duration)
                        }
                        videoView1.start()
                    }
                }
            }
        }
    }
    fun parsePath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Video.Media.DATA)
        val cursor: Cursor? = contentResolver.query(uri!!, projection, null, null, null)
        return if (cursor != null) {
            val columnIndex: Int = cursor
                .getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)
        } else null
    }
}